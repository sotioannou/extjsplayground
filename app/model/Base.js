Ext.define('ExtjsPlayground.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'ExtjsPlayground.model'
    }
});
