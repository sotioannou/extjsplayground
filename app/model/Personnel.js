Ext.define('ExtjsPlayground.model.Personnel', {
    extend: 'ExtjsPlayground.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});
