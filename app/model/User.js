/**
 * Created by sotiris on 7/29/17.
 */
Ext.define('ExtjsPlayground.model.User', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'id', type: 'int'},
        {name: 'name', type: 'string'},
        {name: 'username', type: 'string'},
        {name: 'email', type: 'string'},
        {name: 'street', type: 'string'},
        {name: 'suite', type: 'string'},
        {name: 'city', type: 'string'},
        {name: 'phone', type: 'string'}
    ],
    proxy: {
        type: 'ajax',
        url: 'https://jsonplaceholder.typicode.com/users',
        reader: {
            type: 'json'
        }
    }
});