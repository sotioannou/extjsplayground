/**
 * Created by sotiris on 8/5/17.
 */
Ext.define('ExtjsPlayground.view.navigation.NavigationView', {
    extend: 'Ext.Toolbar',
    xtype: 'app-navigation',

    requires: [
        'Ext.Button'
    ],

    controller: 'navigation',
    items: [
        {
            xtype: 'button',
            text: 'Home',
            flex: 1,
            reference: 'app-center',
            handler: 'buttonClicked'
        },
        {
            xtype: 'button',
            text: 'About',
            flex: 1,
            reference: 'app-about',
            handler: 'buttonClicked'
        },
        {
            xtype: 'button',
            text: 'Button 2',
            flex: 1
        }
    ]
});