/**
 * Created by sotiris on 8/5/17.
 */
Ext.define('ExtjsPlayground.view.navigation.NavigationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.navigation',

    routes: {
        'about': 'navigateTo',
        'home': 'navigateTo'
    },
    buttonClicked: function(token) {
        const routeToLowerCase = Ext.util.Format.lowercase(token.config.text);
        const viewport = this.getView().up('app-home');
        const currentItem = viewport.lookupReference('app-holder').getItems().items[0].xtype;
        //check what currently is inside viewport
        if (!currentItem.localeCompare(token.getReference()) > -1) {
            //remove item from app center
            viewport.lookupReference('app-holder').removeAll(true, true);
            viewport.lookupReference('app-holder').add({
                xtype: token.getReference()
            });
            this.redirectTo(routeToLowerCase);
        }
    },
    navigateTo: function () {
        const self = this;
        const location = window.location.hash.substring(1);

        if (location.localeCompare('home') > -1) {
            self.buttonClicked({
                getReference: function() {
                    return 'app-' + 'center'
                },
                config: {
                    text: 'home'
                }
            });
        }
        else {
            self.buttonClicked({
                getReference: function () {
                    return 'app-' + location;
                },
                config: {
                    text: location
                }
            });
        }

    }


    // navigateTo: function() {
    //     const self = this;
    //     const location = window.location.hash.substring(1);
    //     if (location.localeCompare('home') > -1) {
    //         self.buttonClicked({
    //             getReference: function() {
    //                 return 'app-' + 'center'
    //             },
    //             config: {
    //                 text: 'home'
    //             }
    //         });
    //     }
    //     else {
    //         self.buttonClicked({
    //             getReference: function () {
    //                 return 'app-' + location
    //             },
    //             config: {
    //                 text: location
    //             }
    //         });
    //     }
    // }
});