/**
 * Created by sotiris on 7/30/17.
 */
Ext.define('ExtjsPlayground.view.home.HomeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.home',

    requires: [
        'ExtjsPlayground.store.UsersStore',
        'ExtjsPlayground.model.User'
    ],

    /**
     * Called when the view is created
     */
    init: function() {
        let self = this;

        // self.getView().destroy();
    }
});