/**
 * Created by sotiris on 7/29/17.
 */
Ext.define('ExtjsPlayground.view.home.HomeView', {
    extend: 'Ext.Container',
    xtype: 'app-home',

    requires: [
        'ExtjsPlayground.view.sharedComponents.CarouselView',
        'ExtjsPlayground.view.home.HomeController',
        'ExtjsPlayground.view.navigation.NavigationView',
        'ExtjsPlayground.view.search.SearchView'
    ],
    controller: 'home',
    scrollable: 'y',
    padding: 20,

    items: [
        {
            xtype: 'container',
            layout: 'hbox',
            minHeight: 1000,
            items: [
                {
                    xtype: 'container',
                    layout: 'vbox',
                    flex: 2,
                    items: [
                        {
                          xtype: 'app-navigation'
                        },
                        {
                            xtype: 'app-carousel'
                        },
                        {
                            xtype: 'app-search',
                        },
                        {
                            xtype: 'container',
                            reference: 'app-holder',
                            items: [
                                {
                                    xtype: 'app-center'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    html: 'This is column 2',
                    style: {
                        backgroundColor: '#f1f8fb'
                    }
                }
            ]
        }
    ]
});