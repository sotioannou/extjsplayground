/**
 * Created by sotiris on 8/5/17.
 */
Ext.define('ExtjsPlayground.view.about.AboutView', {
    extend: 'Ext.Container',
    xtype: 'app-about',

    html: 'about page'
});