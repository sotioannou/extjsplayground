/**
 * Created by sotiris on 8/6/17.
 */
Ext.define('ExtjsPlayground.view.popupForMainGrid.PopupWindow', {
    extend: 'Ext.Dialog',
    xtype: 'app-popupwindow',
    controller: 'popupwindow',
    viewModel: 'popupwindow',
    title: 'Person\'s data',

    maximizable: true,
    items: [
        {
            xtype: 'container',
            layout: 'hbox',
            reference: 'popupwindow-container',
            defaults: {
              flex: 1,
              layout: {
                  type: 'vbox',
                  pack: 'center'
              }
            },
            items: [
                {
                    xtype: 'container',
                    items: [
                        {
                            xtype: 'label',
                            bind: {
                                html: '{name}'
                            }
                        },
                        {
                            xtype: 'label',
                            bind: {
                                html: '{username}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    items: [
                        {
                            xtype: 'label',
                            bind: {
                                html: '{email}'
                            }
                        },
                        {
                            xtype: 'label',
                            bind: {
                                html: '{website}'
                            }
                        }
                    ]
                }
            ]
        }
    ],
    buttons: {
        ok: function () {  // standard button (see below)
            const self = this;
            self.up('app-popupwindow').destroy();
        },
    },
    listeners: {
        updatedata: 'onUpdateData'
    }

});