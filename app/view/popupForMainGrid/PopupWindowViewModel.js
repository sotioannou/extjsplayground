/**
 * Created by s.ioannou on 8/9/2017.
 */
Ext.define('ExtjsPlayground.view.popupForMainGrid.PopupWindowViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.popupwindow',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'PopupWindow',
            autoLoad: true
        }
        */
    },

    data: {
        name: null,
        username: null,
        email: null,
        website: null
    }
});