/**
 * Created by sotiris on 8/7/17.
 */
Ext.define('ExtjsPlayground.view.search.SearchView', {
    extend: 'Ext.field.Search',
    xtype: 'app-search',
    requires: [
        'ExtjsPlayground.view.search.SearchController'
    ],
    controller: 'search',

    label: 'Search',
    margin: 20,
    listeners: {
        action: 'onAction'
    }
});