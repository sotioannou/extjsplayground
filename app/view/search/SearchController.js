/**
 * Created by sotiris on 8/7/17.
 */
Ext.define('ExtjsPlayground.view.search.SearchController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.search',

    onAction: (self, event, options) => {
        const theUsersStore = Ext.getStore('Users');
        const theCommentsStore = Ext.getStore('Comments');

        theUsersStore.filter('username', self.getValue());
        console.log(theUsersStore.getData().items);
    }
});