/**
 * Created by sotiris on 7/29/17.
 */
Ext.define('ExtjsPlayground.view.sharedComponents.CarouselView', {
    extend: 'Ext.carousel.Carousel',
    xtype: 'app-carousel',

    height: 300,
    items: [
        {
            html : 'Item 1',
            style: 'background-color: #5E99CC'
        },
        {
            html : 'Item 2',
            style: 'background-color: #759E60'
        },
        {
            html : 'Item 3'
        }
    ]
});