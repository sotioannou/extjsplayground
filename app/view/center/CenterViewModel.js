/**
 * Created by sotiris on 8/7/17.
 */
Ext.define('ExtjsPlayground.view.center.CenterViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.center',

    stores: {
        Users: {
            model: 'ExtjsPlayground.model.User',
            autoLoad: true
        }
        // Users: {
        //     model: 'ExtjsPlayground.model.User',
        //     autoLoad: true
        // }
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'Center',
            autoLoad: true
        }
        */
    },

    data: {
        title: 'Users',
        storeId: 'Users'
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    },

});