/**
 * Created by sotiris on 8/1/17.
 */
Ext.define('ExtjsPlayground.view.center.CenterController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.center',

    requires: [
        'Ext.grid.Grid'
    ],

    /**
     * Called when the view is created
     */
    init: function() {
        //add store to the grid when ready
        // const self = this;
        // self.addGridToView();

    },

    // onShowTest: function (sender, options) {
    //     const self = this;
    //     // self.addGridToView();
    // },

    onSelect: (self, location, options) => {
       const name = location[0].data.name;
       const email = location[0].data.email;
       const username = location[0].data.username;
       const website = location[0].data.website;

       const popup = Ext.create({
                        xtype: 'app-popupwindow',
                     }).show();
       popup.getViewModel().setData({
           name: name,
           email: email,
           username: username,
           website: website
       });
    },
    // addGridToView() {
    //     let store = Ext.getStore('Users');
    //     const self = this;
    //     store.load((records, operation, success) => {
    //         // console.log(records);
    //         //add data to the view
    //         if (success) {
    //             self.getView().add(
    //                 Ext.create('Ext.grid.Grid', {
    //                     title: 'Users',
    //                     requires: ['ExtjsPlayground.view.popupForMainGrid.PopupWindow'],
    //                     store: store,
    //                     columns: [
    //                         {text: 'Name', dataIndex: 'name', flex: 2},
    //                         {text: 'Username', dataIndex: 'username', flex: 1},
    //                         {text: 'Website', dataIndex: 'website', flex: 1}
    //                     ],
    //                     height: 500,
    //                     listeners: {
    //                         'select': (self, location, options) => {
    //
    //                         }
    //                     }
    //                 })
    //             )
    //         }
    //     });
    // }
});