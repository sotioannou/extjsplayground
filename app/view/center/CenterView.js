/**
 * Created by sotiris on 8/1/17.
 */
Ext.define('ExtjsPlayground.view.center.CenterView', {
    extend: 'Ext.grid.Grid',
    xtype: 'app-center',

    requires: [
        'ExtjsPlayground.view.center.CenterController',
        'ExtjsPlayground.view.center.CenterViewModel'
    ],
    controller: 'center',
    viewModel: 'center',
    bind: {
        title: '{title}',
        store: '{storeId}'
    },
    columns: [
        {text: 'Name', dataIndex: 'name', flex: 2},
        {text: 'Username', dataIndex: 'username', flex: 1},
        {text: 'Website', dataIndex: 'website', flex: 1}
    ],
    height: 500,
    listeners: {
        select: 'onSelect'
    }

    // store: 'Users',
    // listeners: {
    //     painted: 'onShowTest'
    // }
});