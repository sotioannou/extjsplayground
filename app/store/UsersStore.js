/**
 * Created by sotiris on 7/30/17.
 */
Ext.define('ExtjsPlayground.store.UsersStore', {
    extend: 'Ext.data.Store',
    requires: [
        'ExtjsPlayground.model.User',
        'Ext.data.ProxyStore'
    ],
    storeId: 'Users',
    autoLoad: true,
    model: 'ExtjsPlayground.model.User',

    // listeners: {
    //     load: function(self, records, successfull, operation) {
    //         if (successfull) {
    //             console.log('The operation is successful and the values are: ', records)
    //
    //         }
    //     }
    // }

    /*
    Uncomment to use a specific model class
    model: 'User',
    */

    /*
    Fields can also be declared without a model class:
    fields: [
        {name: 'firstName', type: 'string'},
        {name: 'lastName',  type: 'string'},
        {name: 'age',       type: 'int'},
        {name: 'eyeColor',  type: 'string'}
    ]
    */

    /*
    Uncomment to specify data inline */
    // data : [
    //     {firstName: 'Ed',    lastName: 'Spencer'},
    //     {firstName: 'Tommy', lastName: 'Maintz'},
    //     {firstName: 'Aaron', lastName: 'Conran'}
    // ]

});