/**
 * Created by s.ioannou on 8/7/2017.
 */
Ext.define('ExtjsPlayground.store.CommentsStore', {
    extend: 'Ext.data.Store',
    requires: [
        'ExtjsPlayground.model.Comments',
        'Ext.data.ProxyStore'
    ],
    storeId: 'Comments',
    model: 'ExtjsPlayground.model.Comments',
    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: 'https://jsonplaceholder.typicode.com/comments',
        reader: {
            type: 'json',
        }
    },
    listeners: {
        load: (self, records, successful, operation, options) => {

        }
    }

    /*
    Uncomment to use a specific model class
    model: 'User',
    */

    /*
    Fields can also be declared without a model class:
    fields: [
        {name: 'firstName', type: 'string'},
        {name: 'lastName',  type: 'string'},
        {name: 'age',       type: 'int'},
        {name: 'eyeColor',  type: 'string'}
    ]
    */

    /*
    Uncomment to specify data inline
    data : [
        {firstName: 'Ed',    lastName: 'Spencer'},
        {firstName: 'Tommy', lastName: 'Maintz'},
        {firstName: 'Aaron', lastName: 'Conran'}
    ]
    */
});