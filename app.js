/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'ExtjsPlayground.Application',

    name: 'ExtjsPlayground',

    requires: [
        // This will automatically load all classes in the ExtjsPlayground namespace
        // so that application classes do not need to require each other.
        'ExtjsPlayground.*'
    ],
    stores: [
      'UsersStore', 'CommentsStore'
    ],

    // The name of the initial view to create.
    mainView: 'ExtjsPlayground.view.home.HomeView'
});
